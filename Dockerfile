FROM almalinux:9

# set version for s6 overlay
ARG S6_DOWNLOAD=https://github.com/just-containers/s6-overlay/releases/download
ARG S6_OVERLAY_VERSION="3.1.6.2"
ARG S6_OVERLAY_ARCH="x86_64"

# add s6 overlay
ADD ${S6_DOWNLOAD}/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz
ADD ${S6_DOWNLOAD}/v${S6_OVERLAY_VERSION}/s6-overlay-${S6_OVERLAY_ARCH}.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-${S6_OVERLAY_ARCH}.tar.xz

# add s6 optional symlinks
ADD ${S6_DOWNLOAD}/v${S6_OVERLAY_VERSION}/s6-overlay-symlinks-noarch.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-symlinks-noarch.tar.xz
ADD ${S6_DOWNLOAD}/v${S6_OVERLAY_VERSION}/s6-overlay-symlinks-arch.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-symlinks-arch.tar.xz

ENTRYPOINT ["/init"]

# Runtime stage
# FROM scratch
# COPY --from=rootfs-stage /root-out/ /
ARG BUILD_DATE
ARG VERSION
LABEL build-version="itk-demo-sw version:- ${VERSION} Build-date:- ${BUILD_DATE}"
LABEL maintainer="gbrandt"

# docker mods and package install scripts from linuxserver.io -- vendorized
# ARG MODS_VERSION="v3"
# ARG PKG_INST_VERSION="v1"
# ADD --chmod=744 "https://raw.githubusercontent.com/linuxserver/docker-mods/mod-scripts/docker-mods.${MODS_VERSION}" "/docker-mods"
# ADD --chmod=744 "https://raw.githubusercontent.com/linuxserver/docker-mods/mod-scripts/package-install.${PKG_INST_VERSION}" "/etc/s6-overlay/s6-rc.d/init-mods-package-install/run"

# environment variables for root
ENV PS1="$(whoami)@$(hostname):$(pwd)\\$ " \
    HOME="/root" \
    TERM="xterm-256color" \
    SHELL=/bin/bash \
    S6_CMD_WAIT_FOR_SERVICES_MAXTIME="0" \
    S6_VERBOSITY=2 \
    S6_BEHAVIOUR_IF_STAGE2_FAILS=2 \
    VIRTUAL_ENV=/venv \
    PATH="/venv/bin:$PATH" \
    PYTHONUNBUFFERED=TRUE

# env notes:
# S6_STAGE2_HOOK=/mod-init \ # not needed in baseimage for the time being
# S6_KEEP_ENV=1 # do not set - this ignores /run/s6/container_environment/ !

# set timezone for EL9
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# busybox not in almalinux
# ADD https://busybox.net/downloads/binaries/1.31.0-defconfig-multiarch-musl/busybox-x86_64
# RUN mv busybox-x86_64 busybox && chmod +x busybox

RUN \
  echo "⧇⧇⧇⧇ install base packages ⧇⧇⧇⧇" && \
  dnf -y --setopt=install_weak_deps=False --best install \
    coreutils-common \
    coreutils-single \
    # curl \
    git \
    hostname \
    jq \
    kmod \
    nc \
    passwd \
    procps \
    psmisc \
    sudo \
    time \
    tree \
    which && \
  echo "⧇⧇⧇⧇ create itk user and make our folders ⧇⧇⧇⧇" && \
  useradd -u 1111 -U -d /config -s /bin/false itk && \
  usermod -G users itk && \
  mkdir -p \
    /config \
    /venv && \
  echo "⧇⧇⧇⧇ cleanup ⧇⧇⧇⧇" && \
  dnf autoremove -y && \
  dnf clean all && \
  rm -rf \
    /tmp/*

RUN echo "⧇⧇⧇⧇ setup Python and create /venv ⧇⧇⧇⧇" \
  && python3 -m venv /venv \
  && python3 -m ensurepip \
  && python3 -m pip install --no-cache-dir --upgrade pip wheel setuptools

# add local files (s6-init scripts)
COPY root/ /

WORKDIR /config
CMD ["s6-setuidgid", "itk", "bash", "--login" ]

